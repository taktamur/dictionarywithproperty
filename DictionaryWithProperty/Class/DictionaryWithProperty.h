//
//  DictionaryWithProperty.h
//  DictionaryWithProperty
//
//  license: http://unlicense.org
//

#import <Foundation/Foundation.h>

@interface DictionaryWithProperty : NSMutableDictionary

@end
