//
//  DictionaryWithProperty.m
//  DictionaryWithProperty
//
//  license: http://unlicense.org
//

#import "DictionaryWithProperty.h"

@implementation DictionaryWithProperty{
    NSMutableDictionary *_dic;
}
-(id)init
{
    self = [super init];
    if(self){
        _dic = [[NSMutableDictionary alloc]init];
    }
    return self;
}

#pragma mark - NSDictionary primitive method.
- (NSUInteger)count
{
    return _dic.count;
}
- (id)objectForKey:(id)aKey
{
    return [_dic objectForKey:aKey];
}
- (NSEnumerator *)keyEnumerator
{
    return [_dic keyEnumerator];
}

#pragma mark - NSMutableDictionary primitive method.
- (void)removeObjectForKey:(id)aKey
{
    [_dic removeObjectForKey:aKey];
}
- (void)setObject:(id)anObject forKey:(id <NSCopying>)aKey
{
    [_dic setObject:anObject forKey:aKey];
}

#pragma mark - NSObject method, property suppot.
//- (BOOL)respondsToSelector:(SEL)sel
//{
//    BOOL isSupport = [super respondsToSelector:sel];
//    if( !isSupport ){
//        if( [self.class isPropertyGet:sel] ){
//            // プロパティのGETメソッドはオールOK
//            isSupport = YES;
//        }else if([self.class isPropertySet:sel]){
//            isSupport = YES;
//        }
//    }
//    return isSupport;
//}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)sel {
    SEL alternateSelector = sel;
    if( [self.class isPropertyGet:sel]){
        alternateSelector = @selector(objectForKey:);
    }else if( [self.class isPropertySet:sel]){
        alternateSelector = @selector(setObject:forKey:);
        
    }
    return [super methodSignatureForSelector:alternateSelector];
}

+(BOOL)isPropertyGet:(SEL)sel
{
    NSString *selectorName = NSStringFromSelector(sel);
    NSUInteger paramCount = [[selectorName componentsSeparatedByString:@":"] count]-1;
    if( paramCount == 0 ){
        return YES;
    }else{
        return NO;
    }
}
+(BOOL)isPropertySet:(SEL)sel
{
    NSString *selectorName = NSStringFromSelector(sel);
    NSUInteger paramCount = [[selectorName componentsSeparatedByString:@":"] count]-1;
    if (paramCount == 1 && [selectorName hasPrefix:@"set"] && selectorName.length > 4) {
        return YES;
    }else{
        return NO;
    }
}

-(void)forwardInvocation:(NSInvocation *)anInvocation
{
    
    NSString *selectorName = NSStringFromSelector([anInvocation selector]);
    NSUInteger paramCount = [[selectorName componentsSeparatedByString:@":"] count]-1;
    if (paramCount == 0) {
        // Getterとみなす。
        // self.hogehoge → [self hogehoge] の変換はコンパイラが行う。
        // ここでは、[self hogehoge] を [self objectForKey:@"hogehoge"] に転送する。
        NSString *propertyName = NSStringFromSelector([anInvocation selector]);
        [anInvocation setArgument:&propertyName atIndex:2];
        anInvocation.selector = @selector(objectForKey:);

        [anInvocation invokeWithTarget:self];
        
    } else if (paramCount == 1 && [selectorName hasPrefix:@"set"] && selectorName.length > 4) {
        // Setterとみなす。
        // self.hogehoge=value → [self setHogehoge:value] の変換はコンパイラが行う。
        // ここでは [self setHogehoge:value] を [self setObject:value forKey:@"hogehoge"]に転送する。
        NSMutableString *propertyName = [NSMutableString stringWithString:NSStringFromSelector([anInvocation selector])];
        [propertyName deleteCharactersInRange:NSMakeRange(0, 3)];                       // setを除去
        [propertyName deleteCharactersInRange:NSMakeRange(propertyName.length - 1, 1)]; // 末尾の:を除去
        
        NSString *firstChar = [[propertyName substringWithRange:NSMakeRange(0,1)] lowercaseString];
        [propertyName replaceCharactersInRange:NSMakeRange(0, 1) withString:firstChar];
        [anInvocation setArgument:&propertyName atIndex:3];
        anInvocation.selector = @selector(setObject:forKey:);

        [anInvocation invokeWithTarget:self];
        
    }else{
        // プロパティアクセスではないとみなす
        [super forwardInvocation:anInvocation];
    }
    
}

@end
