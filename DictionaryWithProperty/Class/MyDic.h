//
//  MyDic.h
//  DictionaryWithProperty
//
// license: http://unlicense.org
//

#import "DictionaryWithProperty.h"

@interface MyDic : DictionaryWithProperty
@property(nonatomic)NSString *appid;
@property(nonatomic)NSString *str;
@property(nonatomic)int integer;

@end
