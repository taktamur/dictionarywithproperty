//
//  main.m
//  DictionaryWithProperty
//
// license: http://unlicense.org
//

#import <Foundation/Foundation.h>
#import "MyDic.h"

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        
        // insert code here...
        NSLog(@"Hello, World!");
        
        MyDic *dic = [[MyDic alloc]init];

        // Dictionaryのようなアクセスも出来る
        [dic setObject:@"hogeValue" forKey:@"hoge"];
        [dic setValue:@"hogeValue2" forKey:@"hoge2"];
        NSLog(@"%@ %@",[dic objectForKey:@"hoge"],[dic valueForKey:@"hoge2"]);

        // プロパティアクセスも出来る!
        dic.appid = @"appidValue";
        dic.str = @"strValue";
        dic.integer = 1;
        NSLog( @"%@ %@ %d",dic.appid, dic.str, dic.integer);

        // プロパティで追加したデータをDictionaryみたいにとれるよ！
        for (id obj in [dic keyEnumerator]) {
            NSLog(@"%@",obj);
        }
        if( [dic isKindOfClass:NSDictionary.class] ){
            NSLog( @"でもやっぱりこれDictionaryだよ！");
        }
    }
    return 0;
}

